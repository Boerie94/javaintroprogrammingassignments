package week5_final_assignments.choice2_gff_query;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author Sboersma
 */
public final class GffQuery {
    /**
     * @param args the command line arguments
     * @throws IOException java.io.IOException
     */
    public static void main(final String[] args) throws IOException {
        GffQuery mainObject = new GffQuery();
        mainObject.start(args);
    }

    /**
     * private constructor.
     */
    private GffQuery() { }
    /**
     * starts the application.
     * When arguments are given in the command line the
     * given arguments will be used. If in netbeans and
     * testing, use the else. At the last case, please
     * uncomment the rules at else.
     * @param args String[]
     * @throws IOException java.io.IOException
     */
    private void start(final String[] args) throws IOException {
        //application logic goes here
        FetchArguments arguments = new FetchArguments();
        if (!arguments.checkNumbersOfArguments(args).isEmpty()) {
            FetchFromFile fff = new FetchFromFile();
            fff.runApplication(arguments.checkNumbersOfArguments(args));
        } else {
            InNetbeansTestQuery inbtq = new InNetbeansTestQuery();
            FetchFromFile fff = new FetchFromFile();
            HashMap testArguments = inbtq.createTestMap();
            //turn this on when testing in netbeans
//            fff.runApplication(testArguments);
        }
    }
}
package week5_final_assignments.choice2_gff_query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Sboersma
 */
public final class Filter {

    /**
     * getSummary() updates the summaryMap. When the key
     * of the summaryMap is found, the value will be added
     * with 1. if the key doesn't exist, a key and value
     * pair is created.
     *
     * @param summaryMap HashMap
     * @param valuesFromFile HashMap
     * @return HashMap
     */
    public HashMap getSummary(final HashMap summaryMap,
            final HashMap valuesFromFile) {
        String total = summaryMap.get("total").toString();
        int totalInt = Integer.parseInt(total);
        int updatedTotal = totalInt + 1;
        if (summaryMap.containsKey(valuesFromFile.get("feature"))) {
            String value = summaryMap.get(valuesFromFile
                    .get("feature")).toString();
            int intValue = Integer.parseInt(value);
            int updatedValue = intValue + 1;
            summaryMap.put(valuesFromFile.get("feature"),
                    Integer.toString(updatedValue));
            summaryMap.put("total", Integer.toString(updatedTotal));
        } else {
            summaryMap.put(valuesFromFile.get("feature"), "1");
            summaryMap.put("total", Integer.toString(updatedTotal));
        }
        return summaryMap;
    }
    /**
     * checkStringOnWildcard() checks a gff3 string on
     * a given regular expression by the user. There
     * is already an existing regular expression so if the user
     * gives a string, it will be fitted in the expression.
     * this function returns true when a match is found.
     *
     * @param gff3 String
     * @param wildcard String
     * @return Boolean
     */

    private Boolean checkStringOnWildcard(final String gff3,
            final String wildcard) {
        if (gff3.contains("name=")) {
            String regex = ".*name=\"(.*" + wildcard + ".*)\"";
            Pattern pt = Pattern.compile(regex);
            Matcher match = pt.matcher(gff3);
            if (match.find()) {
                return true;
            }
        }
        return false;
    }

    /**
     * matchOnChildrenPattern() checks a gff3 string for a
     * parent node, with user input. true is returned when
     * the gff3 string contains the pattern given by the user.
     *
     * @param childrenInput String
     * @param gff3 String
     * @return Boolean
     */

    private Boolean matchOnChildrenPattern(final String childrenInput,
            final String gff3) {
        return gff3.contains("Parent=" + childrenInput);
    }

    /**
     * in fetchRegion(), the start en end coordinates given by user input,
     * will be compared to the start and end coordinates found in
     * the input file. when the coordinates from the file laying in between
     * the coordinates given by the user, true is returned.
     *
     * @param coordinates String
     * @param start int
     * @param end int
     * @return Boolean
     */

    private Boolean fetchRegion(final String coordinates,
            final int start,
            final int end) {
        int startcoordinate = Integer.parseInt(coordinates.split("\\..")[0]);
        int endcoordinate = Integer.parseInt(coordinates.split("\\..")[1]);
        return (startcoordinate <= start && endcoordinate >= end);
    }

    /**
     * getDifferenceStartEnd() calculates the difference (length) between
     * the end coordinate and startcoordinate from the input file. the
     * length is returned.
     *
     * @param start String
     * @param end String
     * @return length Integer
     */

    private int getDifferenceStartEnd(final String start, final String end) {
        int startInt = Integer.parseInt(start);
        int endInt = Integer.parseInt(end);
        int length = endInt - startInt;
        return length;
    }

    /**
     * checkOnMinValue() is comparing the length calculated in/by
     * getDifferenceStartEnd() with the min. length the user
     * has given. true is returned if the min. length
     * (user input) is a "*" or when the min. length is not a
     * "*" and is smaller then calulated length.
     *
     * @param length int
     * @param minLengthArgument String
     * @return Boolean
     */

    private Boolean checkOnMinValue(final int length,
            final String minLengthArgument) {
        if (!minLengthArgument.equals("*")) {
            int inputMin = Integer.parseInt(minLengthArgument);
            return inputMin < length;
        }
        return true;
    }

    /**
     * checkOnMaxValue() is comparing the length calculated in/by
     * getDifferenceStartEnd() with the max. length the user
     * has given. true is returned if the max. length
     * (user input) is a "*" or when the max. length is not a
     * "*" and is smaller then calulated length.
     *
     * @param length int
     * @param maxLengthArgument String
     * @return Boolean
     */

    private Boolean checkOnMaxValue(final int length,
            final String maxLengthArgument) {
        if (!maxLengthArgument.equals("*")) {
            int inputMax = Integer.parseInt(maxLengthArgument);
            return inputMax > length;
        }
        return true;
    }

    /**
     * splitFilterArgument() splits the filter argument,
     * given by user input. A new ArrayList is made with
     * the extracted values from the filter argument.
     * the "*|*|*|*|*" format is now a ArrayList and
     * is returned
     *
     * @param arguments HashMap
     * @return ArrayList
     */

    private ArrayList splitFilterArgument(final HashMap arguments) {
        String filterParameters = arguments.get("filter").toString();
        for (String split : filterParameters.split("\\|")) {
            int i = 0;
            ArrayList<String> filterOptions = new ArrayList<>();
            filterOptions.add(filterParameters.split("\\|")[i]);
            i += 1;
            filterOptions.add(filterParameters.split("\\|")[i]);
            i += 1;
            filterOptions.add(filterParameters.split("\\|")[i]);
            i += 1;
            filterOptions.add(filterParameters.split("\\|")[i]);
            i += 1;
            filterOptions.add(filterParameters.split("\\|")[i]);
            i += 1;
            return filterOptions;
        }
        return null;
    }

    /**
     * filterHashMapWithFilterArgument() is using the formed ArrayList
     * by splitFilterArgument() to check all elements from that ArrayList.
     * true is returned when all the if statements are false. when true
     * is returned, all the elements from the ArrayList are matched with
     * the same elements from the input file. If element like "*" matches
     * with everything
     *
     * @param resultMap HashMap
     * @param filterOptions ArrayList
     * @return Boolean
     */

    private Boolean filterHashMapWithFilterArgument(final HashMap resultMap,
            final ArrayList filterOptions) {
        int length = getDifferenceStartEnd(resultMap.get("start").toString(),
                resultMap.get("end").toString());
        if (!checkOnMinValue(length,
                filterOptions.get(4).toString())) {
            return false;
        } else if (!checkOnMaxValue(length,
                filterOptions.get(3).toString())) {
            return false;
        } else if (!resultMap.get("strand").toString()
                .equals(filterOptions.get(2).toString())
                && !filterOptions.get(2).equals("*")) {
            return false;
        } else if (!resultMap.get("score").toString()
                .equals(filterOptions.get(1).toString())
                && !filterOptions.get(1).equals("*")) {
            return false;
        } else if (!resultMap.get("source").toString().toLowerCase()
                .equals(filterOptions.get(0).toString().toLowerCase())
                && !filterOptions.get(0).equals("*")) {
            return false;
        }
        return true;
    }

    /**
     * filterLine() checks the arguments given by user input.
     * Functions are calles in this function depending on
     * which arguments are given by the user. When all if statements
     * are false, true is returned. So all the user arguments
     * matches with the same elements from the file. However, when
     * the summary argument is given, there is only a summery given.
     *
     * @param valuesFromFile HashMap
     * @param arguments HashMap
     * @return Boolean
     */

    public Boolean filterLine(final HashMap valuesFromFile,
            final HashMap arguments) {
            if (arguments.containsKey("filter")
                    && !filterHashMapWithFilterArgument(
                            valuesFromFile, splitFilterArgument(arguments))) {
                return false;
            } else if (arguments.containsKey("fetch_type")
                    && !arguments.get("fetch_type").toString().toLowerCase()
                        .equals(valuesFromFile.get("feature")
                                .toString().toLowerCase())) {
                return false;
            } else if (arguments.containsKey("fetch_region")
                    && !fetchRegion(arguments.get("fetch_region").toString(),
                            Integer.parseInt((String)
                                    valuesFromFile.get("start")),
                            Integer.parseInt((String)
                                    valuesFromFile.get("end")))) {
                return false;
            } else if (arguments.containsKey("fetch_children")
                    && !matchOnChildrenPattern(arguments.get(
                                    "fetch_children").toString(),
                            valuesFromFile.get("GFF3").toString())) {
                return false;
            } else {
                return !(arguments.containsKey("fetch_wildcard")
                        && !checkStringOnWildcard(valuesFromFile.get("GFF3")
                                .toString().toLowerCase(),
                                arguments.get("fetch_wildcard")
                                        .toString().toLowerCase()));
                }
    }
}
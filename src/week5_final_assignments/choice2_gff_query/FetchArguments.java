/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice2_gff_query;

import java.util.HashMap;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * @author Sboersma
 */
public final class FetchArguments {
    /**
     * ARGUMENTSMAP is a global HashMap to use it in different functions.
     */

    private static final HashMap<String, String> ARGUMENTSMAP = new HashMap<>();

    /**
     * checkNumbersOfArguments() checks all the given arguments by the user.
     * A help option is displayed when a wrong argument is given or when
     * a wrong format is given to a certain argument. When all checks are
     * passed, the filled ARGUMENTSMAP is returned. when an error occured,
     * a empty ARGUMENTSMAP is returned and a error massage will be printed.
     *
     * @param args String[]
     * @return HashMap
     */

    public HashMap checkNumbersOfArguments(final String[] args) {
        HelpFormatter formatter = new HelpFormatter();
        Options options = new Options();
        options.addOption("h", "help", false, "help");
        options.addOption("i", "infile", true,
                "specific the input file (.gff3) format");
        options.addOption("ft", "fetch_type", true,
                "specific input type (A-Za-z_0-9)");
        options.addOption("fc", "fetch_children", true,
                "specific children nodes (A-Za-z_0-9)");
        options.addOption("fr", "fetch_region", true,
                "specific a region like ([0-9]..[0-9])");
        options.addOption("f", "filter", true,
                "source|score|strand|max|min like '*|*|+|10000|*'");
        options.addOption("fw", "fetch_wildcard", true,
                "give string to search a gff3 strand for a certain name\n"
                        + "only use: [A-Za-z_0-9][|]");
        options.addOption("sum", "summary", true,
                "please fill in true behind the argument to show a summary");
        try {
            BasicParser parser = new BasicParser();
            CommandLine cli = parser.parse(options, args);
            if (cli.hasOption("h")) {
                formatter.printHelp("GFF3 parser", options);
            } else {
                if (!checkOptionI(cli.getOptionValue("i"),
                    cli.hasOption("i"))) {
                System.out.println("there is no (correct) input file given!");
                formatter.printHelp("GFF3 parser", options);
            }
            if (!checkIfMatches(cli.getOptionValue("fr"),
                                cli.hasOption("fr"), "\\d+\\.+\\d+",
                                "fetch_region")
                        || !checkIfMatches(cli.getOptionValue("fc"),
                                cli.hasOption("fc"), "\\w+", "fetch_children")
                        || !checkIfMatches(cli.getOptionValue("fw"),
                                cli.hasOption("fw"), "[\\d|\\w|\\||\\[|\\]]+",
                                "fetch_wildcard")
                        || !checkIfMatches(cli.getOptionValue("ft"),
                                cli.hasOption("ft"), "\\w+", "fetch_type")
                        || !checkIfMatches("true",
                                cli.hasOption("sum"), "true", "summary")
                        || !checkIfMatches(cli.getOptionValue("f"),
                                cli.hasOption("f"),
                                "[\\*|\\w]+\\|[\\*|\\d]+\\|[\\*|+|-]"
                                        + "\\|[\\d|+\\*]+\\|[\\*|\\d]+",
                                "filter")) {
                    System.out.println("An error was occured, "
                            + "please give the right format!");
                    formatter.printHelp("GFF3 parser", options);
                    ARGUMENTSMAP.clear();
                    return ARGUMENTSMAP;
                } else {
                formatter.printHelp("GFF3 parser", options);
            }
            }
        } catch (ParseException e) {
            formatter.printHelp("GFF3 parser", options);
        }
        return ARGUMENTSMAP;
    }

    /**
     * checkOptionI() checks the value of user argument -i. ARGUMENTSMAP
     * will be filled with the fileName if it has the correct extension.
     * True will be returned when the file extension is correct.
     *
     * @param optionI String
     * @param hasI Boolean
     * @return Boolean
     */
    private Boolean checkOptionI(final String optionI, final Boolean hasI) {
        if (hasI) {
            if (optionI.endsWith(".gff3")) {
                ARGUMENTSMAP.put("infile", optionI);
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * checkIfMatches() checks if the argument even exists
     * and if the user input matches the required pattern.
     * If both are true, true is returned and the argument
     * with its value is put in the ARGUMENTSMAP. False will
     * be returned in other cases.
     *
     * @param argumentValue String
     * @param hasArgument Boolean
     * @param pattern String
     * @param arg String
     * @return Boolean
     */
    private Boolean checkIfMatches(final String argumentValue,
            final Boolean hasArgument,
            final String pattern,
            final String arg) {
        if (hasArgument) {
            if (argumentValue.matches(pattern)) {
                ARGUMENTSMAP.put(arg, argumentValue);
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}

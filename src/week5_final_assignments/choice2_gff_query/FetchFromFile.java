/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice2_gff_query;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Sboersma
 */

public final class FetchFromFile {

    /**
     * getAllInformationFromFile() matches a regular expression
     * on a line from the input file. A Matcher object is returned
     * to get the information from that object.
     *
     * @param line String
     * @return match Matcher
     */

    private Matcher getAllInformationFromFile(final String line) {
        String regex = "([\\w\\.]+)[\t\\s]+([\\w|\\.\\/]+)[\t\\s]+"
                + "([\\w]+)[\t|\\s]+(\\d+)[\t\\s]+(\\d+)[\\s|\t]+"
                + "([\\.|\\d])+[\t\\s]+([\\.|\\+|\\-])[\t|\\s]+"
                + "([\\.|\\d])[\\s|\\t]+(.*)";
        Pattern pt = Pattern.compile(regex);
        Matcher match = pt.matcher(line);
        return match;
    }

    /**
     * createResultHashmapWithValuesFromFile() uses the Matcher object
     * created by getAllInformationFromFile() to get all the needed information
     * from the input file. A HashMap is created with the values from the file.
     *
     * @param match Matcher
     * @param translationHashmap HashMap
     * @return HashMap
     */

    private HashMap createResultHashmapWithValuesFromFile(final Matcher match,
            final HashMap translationHashmap) {
        HashMap<String, String> valuesFromFile = new HashMap<>();
        while (match.find()) {
            for (int i = 1; i <= match.groupCount(); i += 1) {
                valuesFromFile.put(translationHashmap.get(i).toString(),
                        match.group(i));
            }
        }
        return valuesFromFile;
    }

    /**
     * createTranslationHashMap() returnes a HashMap with numbers as keys
     * and a value, which is gonna be the key in the HashMap from
     * createResultHashmapWithValuesFromFile().
     *
     * @return numbersToKeys
     */

    private HashMap createTranslationHashMap() {
        HashMap<Integer, String> numbersToKeys = new HashMap<>();
        int number = 1;
        numbersToKeys.put(number, "seqname");
        number += 1;
        numbersToKeys.put(number, "source");
        number += 1;
        numbersToKeys.put(number, "feature");
        number += 1;
        numbersToKeys.put(number, "start");
        number += 1;
        numbersToKeys.put(number, "end");
        number += 1;
        numbersToKeys.put(number, "score");
        number += 1;
        numbersToKeys.put(number, "strand");
        number += 1;
        numbersToKeys.put(number, "frame");
        number += 1;
        numbersToKeys.put(number, "GFF3");
        number += 1;
        return numbersToKeys;
    }

    /**
     * runApplication() runs the whole application. First the input file,
     * given by the user, is validated. When the file exists and it can
     * be opened, the filters are called. If filter.FilterLine() returns true,
     * the line will be printed to the user. If some kind of error takes place,
     * a massage will be shown to the user. If the option summary is given,
     * only the summery is returned,
     * if not, the 'normal' output will be returned
     *
     * @param arguments HashMap
     * @throws IOException java.io.IOException
     */

    public void runApplication(final HashMap arguments) throws IOException {
        try (Scanner file = new Scanner(new FileReader(
                arguments.get("infile").toString()))) {
            HashMap translationHashmap = createTranslationHashMap();
            HashMap<String, String> summaryMap = new HashMap<>();
            summaryMap.put("total", "0");
            while (file.hasNext()) {
                Matcher match = getAllInformationFromFile(file.nextLine());
                HashMap valuesFromFile =
                        createResultHashmapWithValuesFromFile(
                                match, translationHashmap);
                Filter filter = new Filter();
                try {
                    if (arguments.containsKey("summary")) {
                        filter.getSummary(summaryMap, valuesFromFile);
                    } else {
                    if (filter.filterLine(valuesFromFile, arguments)) {
                        System.out.println(
                            valuesFromFile.get("seqname").toString()
                            + valuesFromFile.get("source").toString() + "\t"
                            + valuesFromFile.get("feature").toString() + "\t"
                            + valuesFromFile.get("start").toString() + "\t"
                            + valuesFromFile.get("end").toString() + "\t"
                            + valuesFromFile.get("score").toString() + "\t"
                            + valuesFromFile.get("strand").toString() + "\t"
                            + valuesFromFile.get("frame").toString() + "\t"
                            + valuesFromFile.get("GFF3").toString());
                        }
                    }
                } catch (NullPointerException e) {
                    System.out.println("An error occured during reading "
                            + "the file, please check your file!");
                }
            }
            if (arguments.containsKey("summary")) {
                Iterator it = summaryMap.keySet().iterator();
                System.out.println("file\t\t\t" + arguments.get("infile"));
                System.out.println("molecules with features\t");
                System.out.println("numbers of features\t"
                        + summaryMap.get("total"));
                while (it.hasNext()) {
                    String key = it.next().toString();
                    if (!key.equals("total")) {
                        if (key.length() >= 8) {
                            System.out.println(key + " \t\t "
                                    + summaryMap.get(key));
                        } else {
                            System.out.println(key + " \t\t\t "
                                    + summaryMap.get(key));
                        }
                    }
                }
            }
        } catch (FileNotFoundException | NullPointerException e) {
            System.out.println("file cannot be opened, "
                    + "please give a valid file path!");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice2_gff_query;

import java.util.HashMap;

/**
 *
 * @author Sboersma
 */
public class InNetbeansTestQuery {
    /**
     * private constructor.
     */
    private void inNetbeansTestQuery() { }
    /**
     * @return HashMap
     */
    public final HashMap createTestMap() {
        HashMap<String, String> argumentsMap = new HashMap<>();
        argumentsMap.put("infile", "<filePath>");
        argumentsMap.put("filter", "bestorf|*|+|*|200");
        argumentsMap.put("fetch_type", "cds");
//        argumentsMap.put("fetch_children", "PGSC0003DMT400091746");
//        argumentsMap.put("fetch_region", "0..2500000");
//        argumentsMap.put("fetch_wildcard", "[Kk]inase");
//        argumentsMap.put("summary", "true");
        return argumentsMap;
    }
}
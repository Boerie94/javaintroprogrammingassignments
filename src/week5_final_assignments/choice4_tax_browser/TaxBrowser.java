/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week5_final_assignments.choice4_tax_browser;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class TaxBrowser {

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        TaxBrowser mainObject = new TaxBrowser();
        mainObject.start();
    }

    /**
     * private constructor.
     */
    private TaxBrowser() { }

    /**
     * starts the application.
     */
    private void start() {
        throw new java.lang.UnsupportedOperationException("Not supported yet.");
        //application logic goes here
    }
}